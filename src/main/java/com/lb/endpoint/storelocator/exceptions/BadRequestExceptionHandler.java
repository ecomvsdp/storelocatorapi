package com.lb.endpoint.storelocator.exceptions;


import com.lb.service.storelocator.exceptions.BadRequestException;
import com.lb.util.serviceutils.exception.Error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Exception Handler to handle BadRequest exceptions thrown from service-storelocator.
 */
@Provider
public class BadRequestExceptionHandler implements ExceptionMapper<BadRequestException> {

    private static Logger LOGGER = LoggerFactory.getLogger(BadRequestExceptionHandler.class);

  @Override
  public Response toResponse(BadRequestException exception) {
      LOGGER.error("Bad Request Exception", exception);

    return Response.status(Response.Status.BAD_REQUEST)
            .entity(new Error(exception.getErrorCode(), exception.getMessage()))
            .type(MediaType.APPLICATION_JSON).build();
  }
}
