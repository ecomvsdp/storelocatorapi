package com.lb.endpoint.storelocator.filter;

import com.lb.endpoint.storelocator.StoreLocatorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.UUID;

/**
 * Jersey filter to tap onto the request and response for a request.
 * <p>
 * If there is no request id, an UUID is generated and added to both the request
 * and response headers.
 */
@Provider
public class ApplicationFilter implements ContainerRequestFilter, ContainerResponseFilter, StoreLocatorConstants {

  private final static Logger LOGGER = LoggerFactory.getLogger(ApplicationFilter.class);

  @Override
  public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
          throws IOException {

    // Copy the request headers to response headers as response headers are
    // empty as a part of response context.
    responseContext.getHeaders().add(REQUEST_ID,
            requestContext.getHeaderString(REQUEST_ID));

  }

  @Override
  public void filter(ContainerRequestContext requestContext) throws IOException {

    // Check if a request-id is present on the request headers. If not, set
    // the request-id header("requestId") to a UUID
    if (requestContext.getHeaderString(REQUEST_ID) == null) {
      requestContext.getHeaders().add(REQUEST_ID, UUID.randomUUID().toString());
    }
  }
}
