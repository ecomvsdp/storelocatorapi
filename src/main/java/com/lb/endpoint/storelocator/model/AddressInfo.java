package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


/**
 * AddressInfo
 */
public class AddressInfo {

  private String streetAddress = null;
  private String city = null;
  private String state = null;
  private String postalCode = null;
  private String country = null;
  private String phone = null;
  private String mallName = null;


  @JsonProperty("streetAddress")
  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }


  @JsonProperty("city")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  @JsonProperty("state")
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }


  @JsonProperty("postalCode")
  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }


  @JsonProperty("country")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }


  @JsonProperty("phone")
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  @JsonProperty("mallName")
  public String getMallName() {
    return mallName;
  }

  public void setMallName(String mallName) {
    this.mallName = mallName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddressInfo addressInfo = (AddressInfo) o;
    return Objects.equals(this.streetAddress, addressInfo.streetAddress) &&
            Objects.equals(this.city, addressInfo.city) &&
            Objects.equals(this.state, addressInfo.state) &&
            Objects.equals(this.postalCode, addressInfo.postalCode) &&
            Objects.equals(this.country, addressInfo.country) &&
            Objects.equals(this.phone, addressInfo.phone) &&
            Objects.equals(this.mallName, addressInfo.mallName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(streetAddress, city, state, postalCode, country, phone, mallName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddressInfo {\n");

    sb.append("    streetAddress: ").append(toIndentedString(streetAddress)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    postalCode: ").append(toIndentedString(postalCode)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    mallName: ").append(toIndentedString(mallName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

