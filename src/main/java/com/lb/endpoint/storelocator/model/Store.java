package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Store
 */
public class Store {

  private Integer storeId = null;
  private String brandId = null;
  private StoreInfo storeInfo = null;
  private AddressInfo addressInfo = null;
  private GeoLocationInfo geoLocationInfo = null;
  private List<String> assortments = new ArrayList<String>();


  @JsonProperty("storeId")
  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }


  @JsonProperty("brandId")
  public String getBrandId() {
    return brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }


  @JsonProperty("storeInfo")
  public StoreInfo getStoreInfo() {
    return storeInfo;
  }

  public void setStoreInfo(StoreInfo storeInfo) {
    this.storeInfo = storeInfo;
  }


  @JsonProperty("addressInfo")
  public AddressInfo getAddressInfo() {
    return addressInfo;
  }

  public void setAddressInfo(AddressInfo addressInfo) {
    this.addressInfo = addressInfo;
  }


  @JsonProperty("geoLocationInfo")
  public GeoLocationInfo getGeoLocationInfo() {
    return geoLocationInfo;
  }

  public void setGeoLocationInfo(GeoLocationInfo geoLocationInfo) {
    this.geoLocationInfo = geoLocationInfo;
  }


  @JsonProperty("assortments")
  public List<String> getAssortments() {
    return assortments;
  }

  public void setAssortments(List<String> assortments) {
    this.assortments = assortments;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Store store = (Store) o;
    return Objects.equals(this.storeId, store.storeId) &&
            Objects.equals(this.brandId, store.brandId) &&
            Objects.equals(this.storeInfo, store.storeInfo) &&
            Objects.equals(this.addressInfo, store.addressInfo) &&
            Objects.equals(this.geoLocationInfo, store.geoLocationInfo) &&
            Objects.equals(this.assortments, store.assortments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(storeId, brandId, storeInfo, addressInfo, geoLocationInfo, assortments);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Store {\n");

    sb.append("    storeId: ").append(toIndentedString(storeId)).append("\n");
    sb.append("    brandId: ").append(toIndentedString(brandId)).append("\n");
    sb.append("    storeInfo: ").append(toIndentedString(storeInfo)).append("\n");
    sb.append("    addressInfo: ").append(toIndentedString(addressInfo)).append("\n");
    sb.append("    geoLocationInfo: ").append(toIndentedString(geoLocationInfo)).append("\n");
    sb.append("    assortments: ").append(toIndentedString(assortments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

