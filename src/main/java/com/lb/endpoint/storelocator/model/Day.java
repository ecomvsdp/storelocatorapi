package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;


/**
 * Gets or Sets Day
 */
public enum Day {
  SUNDAY("sunday"),
  MONDAY("monday"),
  TUESDAY("tuesday"),
  WEDNESDAY("wednesday"),
  THURSDAY("thursday"),
  FRIDAY("friday"),
  SATURDAY("saturday");

  private String value;

  Day(String value) {
    this.value = value;
  }

  public static Day getDayByName(String name) {
    for (Day day : values()) {
      if (StringUtils.equals(name, day.value)) {
        return day;
      }
    }

    // Should never get to this point
    return null;
  }


  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }
}

