package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


/**
 * GeoLocationInfo
 */
public class GeoLocationInfo {

  private Double latitudeDeg = null;
  private Double longitudeDeg = null;
  private Double latitudeRad = null;
  private Double longitudeRad = null;


  @JsonProperty("latitudeDeg")
  public Double getLatitudeDeg() {
    return latitudeDeg;
  }

  public void setLatitudeDeg(Double latitudeDeg) {
    this.latitudeDeg = latitudeDeg;
  }


  @JsonProperty("longitudeDeg")
  public Double getLongitudeDeg() {
    return longitudeDeg;
  }

  public void setLongitudeDeg(Double longitudeDeg) {
    this.longitudeDeg = longitudeDeg;
  }


  @JsonProperty("latitudeRad")
  public Double getLatitudeRad() {
    return latitudeRad;
  }

  public void setLatitudeRad(Double latitudeRad) {
    this.latitudeRad = latitudeRad;
  }


  @JsonProperty("longitudeRad")
  public Double getLongitudeRad() {
    return longitudeRad;
  }

  public void setLongitudeRad(Double longitudeRad) {
    this.longitudeRad = longitudeRad;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeoLocationInfo geoLocationInfo = (GeoLocationInfo) o;
    return Objects.equals(this.latitudeDeg, geoLocationInfo.latitudeDeg) &&
            Objects.equals(this.longitudeDeg, geoLocationInfo.longitudeDeg) &&
            Objects.equals(this.latitudeRad, geoLocationInfo.latitudeRad) &&
            Objects.equals(this.longitudeRad, geoLocationInfo.longitudeRad);
  }

  @Override
  public int hashCode() {
    return Objects.hash(latitudeDeg, longitudeDeg, latitudeRad, longitudeRad);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeoLocationInfo {\n");

    sb.append("    latitudeDeg: ").append(toIndentedString(latitudeDeg)).append("\n");
    sb.append("    longitudeDeg: ").append(toIndentedString(longitudeDeg)).append("\n");
    sb.append("    latitudeRad: ").append(toIndentedString(latitudeRad)).append("\n");
    sb.append("    longitudeRad: ").append(toIndentedString(longitudeRad)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

