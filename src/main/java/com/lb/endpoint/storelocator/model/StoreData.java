package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * StoreData
 */
public class StoreData {

  private Store store = null;
  private List<OperationalHours> operationalHours = new ArrayList<OperationalHours>();


  /**
   **/
  public StoreData store(Store store) {
    this.store = store;
    return this;
  }

  @JsonProperty("store")
  public Store getStore() {
    return store;
  }

  public void setStore(Store store) {
    this.store = store;
  }


  /**
   **/
  public StoreData operationalHours(List<OperationalHours> operationalHours) {
    this.operationalHours = operationalHours;
    return this;
  }

  @JsonProperty("operationalHours")
  public List<OperationalHours> getOperationalHours() {
    return operationalHours;
  }

  public void setOperationalHours(List<OperationalHours> operationalHours) {
    this.operationalHours = operationalHours;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoreData storeData = (StoreData) o;
    return Objects.equals(this.store, storeData.store) &&
            Objects.equals(this.operationalHours, storeData.operationalHours);
  }

  @Override
  public int hashCode() {
    return Objects.hash(store, operationalHours);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StoreData {\n");

    sb.append("    store: ").append(toIndentedString(store)).append("\n");
    sb.append("    operationalHours: ").append(toIndentedString(operationalHours)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

