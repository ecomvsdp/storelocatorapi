package com.lb.endpoint.storelocator;

import com.lb.plugin.jetty.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppServer extends Server {

  private static final Logger LOGGER = LoggerFactory.getLogger(AppServer.class);

  private AppServer(boolean startWithSessionShare, String appProviderPackage) {
    super(startWithSessionShare, appProviderPackage);
  }

  public static void main(String[] args) throws Exception {
    try {
      AppServer server = new AppServer(true, "com.lb.endpoint.storelocator");
      servletContextPath = "/storelocator";
      serviceName = "Storelocator Services";
      server.run();
    } catch (Exception e) {
      LOGGER.error("Service Initializing Exception: ", e);
      System.exit(1);
    }
  }
}
