package com.lb.endpoint.storelocator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.lb.data.cache.StoreLocatorCacheNames;
import com.lb.data.storeLocator.AddressInfo;
import com.lb.data.storeLocator.Day;
import com.lb.data.storeLocator.GeoLocationInfo;
import com.lb.data.storeLocator.OperationalHours;
import com.lb.data.storeLocator.OperationalInfo;
import com.lb.data.storeLocator.Store;
import com.lb.data.storeLocator.StoreInfo;
import com.lb.endpoint.storelocator.model.Stores;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EndpointStoreLocatorIntTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(EndpointStoreLocatorIntTest.class);
  private static final String BASE_URI = "http://localhost:8094/storelocator/";
  private static org.glassfish.grizzly.http.server.HttpServer server;
  private static HazelcastInstance storeLocatorHzInstance;
  private static ObjectMapper mapper;
  private static HazelcastInstance auditMetricsHzInstance;
  private static final String SEARCH_BY_KEYWORD_PATH = "VS/v1/stores/search";
  private static final String SEARCH_BY_GEO_LOCATION_PATH = "VS/v1/stores/search/geolocation";
  private static final String FIND_BY_STORE_IDS_PATH = "VS/v1/stores/find";

  @BeforeClass
  public static void beforeClass() {
    System.setProperty("groupName", "local");
    mapper = new ObjectMapper();

    try {
      storeLocatorHzInstance = Hazelcast.newHazelcastInstance(new XmlConfigBuilder(
              new FileInputStream(new File(EndpointStoreLocatorIntTest.class.getClassLoader().getResource("hazelcast-storelocator.xml").getFile())))
              .build());

      auditMetricsHzInstance = Hazelcast.newHazelcastInstance(
              new XmlConfigBuilder(
                      new FileInputStream(
                              new File(EndpointStoreLocatorIntTest.class.getClassLoader().getResource("hazelcast-audit-metrics.xml").getFile())))
                      .build());

      ResourceConfig rc = new ResourceConfig().packages("com.lb.endpoint.storelocator");
      server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);

      // Load Hazelcast data
      loadStoreLocatorAssortments();
      loadStoreLocatorStoreData();
      loadStoreLocatorOperationalHours();
    } catch (FileNotFoundException e) {
      LOGGER.error("Error starting the hazelcast server for storeLocator or auditMetrics " + e.getMessage(), e);
      throw new RuntimeException(e);
    }
  }

  @AfterClass
  public static void After() {
    server.shutdown();
    storeLocatorHzInstance.shutdown();
    auditMetricsHzInstance.shutdown();
  }

  @Test
  public void testSearchStoresByKeywordUsingPostalCode() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"96818"}).request().get(Response.class);

    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"09"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"04"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Honolulu"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"HI"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"96818"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Island Paradise"));

    // GeoLocationInfo Check
    Assert.assertTrue(jsonResult.contains("\"latitudeDeg\":21.315603"));
    Assert.assertTrue(jsonResult.contains("\"longitudeDeg\":-157.858093"));
    Assert.assertTrue(jsonResult.contains("\"latitudeRad\":0.3720275"));
    Assert.assertTrue(jsonResult.contains("\"longitudeRad\":-2.755143474"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"SWIM"));

    // Operational Hours Check
    Assert.assertTrue(jsonResult.contains("weekDay\":\"monday\""));
    Assert.assertTrue(jsonResult.contains("weekDay\":\"wednesday\""));
    Assert.assertFalse(jsonResult.contains("weekDay\":\"friday\""));

  }

  @Ignore
  @Test
  public void testSearchStoresByKeywordUsingCity() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"Honolulu"}).request().get(Response.class);

    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"09"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"04"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Honolulu"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"HI"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"96818"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Island Paradise"));

    // GeoLocationInfo Check
    Assert.assertTrue(jsonResult.contains("\"latitudeDeg\":21.315603"));
    Assert.assertTrue(jsonResult.contains("\"longitudeDeg\":-157.858093"));
    Assert.assertTrue(jsonResult.contains("\"latitudeRad\":0.3720275"));
    Assert.assertTrue(jsonResult.contains("\"longitudeRad\":-2.755143474"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"SWIM"));
  }

  @Test
  public void testSearchStoresByKeywordUsingPostalCodeBad() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"00000"}).request().get(Response.class);

    Assert.assertTrue(response.getStatus() == 404);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("SER-SL-STORES_NOT_FOUND"));
  }

  @Ignore
  @Test
  public void testSearchStoresByKeywordUsingState() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"OH"}).request().get(Response.class);

    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"06"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"01"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Columbus"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"OH"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"43211"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Polaris Parkway"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"PINK"));
  }

  @Ignore
  @Test
  public void testSearchStoresByKeywordUsingMallName() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"Polaris Parkway"}).request().get(Response.class);

    // Validate tests
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"06"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"01"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Columbus"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"OH"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"43211"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Polaris Parkway"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"PINK"));
  }

  @Test
  public void testSearchStoresByKeywordUsingNoKeyword() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.request().get(Response.class);

    // Validate test
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(response.getStatus() == 400);
    Assert.assertTrue(jsonResult.contains("Input Invalid"));
  }

  @Ignore
  @Test
  public void testSearchStoresByKeywordNoStoresFound() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_KEYWORD_PATH);
    Response response = target.queryParam("keyword", new Object[]{"CA"}).request().get(Response.class);

    // Validate test
    Assert.assertTrue(response.getStatus() == 404);

    // Only the empty json brackets should be present
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("STORES_NOT_FOUND"));
  }

  @Test
  public void testSearchStoresByGeoLocInvalidInputDataLatitude() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{134.086245}).
            queryParam("long", new Object[]{-157.858093}).
            queryParam("radius", new Object[]{10.0}).
            request().get(Response.class);

    // Validate test
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(response.getStatus() == 400);
    Assert.assertTrue(jsonResult.contains("SER-SL-INVALID_INPUT"));
  }

  @Test
  public void testSearchStoresByGeoLocInvalidInputDataLongitude() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{21.315603}).
            queryParam("long", new Object[]{-183.130469}).
            queryParam("radius", new Object[]{10.0}).
            request().get(Response.class);

    // Validate test
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(response.getStatus() == 400);
    Assert.assertTrue(jsonResult.contains("SER-SL-INVALID_INPUT"));
  }

  @Test
  public void testSearchStoresByGeoLocInvalidInputDataNegativeRadius() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{21.315603}).
            queryParam("long", new Object[]{-157.858093}).
            queryParam("radius", new Object[]{-500}).
            request().get(Response.class);

    // Validate test
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(response.getStatus() == 400);
    Assert.assertTrue(jsonResult.contains("SER-SL-INVALID_INPUT"));
  }

  @Test
  public void testSearchStoresByGeoLocInvalidInputDataRadiusNaN() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{21.315603}).
            queryParam("long", new Object[]{-157.858093}).
            queryParam("radius", new Object[]{"rad10"}).
            request().get(Response.class);

    // Validate test
    // The error will be thrown by the "framework" given that it, the "framework", tries to convert
    // the incoming query params into the correct type as listed in the method signature.
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(response.getStatus() == 404);
    Assert.assertTrue(jsonResult.contains("Web App Exception Occurred!"));
  }

  @Test
  public void testSearchStoresByGeoLocNoStoresFound() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{0.00}).
            queryParam("long", new Object[]{0.00}).
            queryParam("radius", new Object[]{20.5}).
            request().get(Response.class);

    // Validate test
    Assert.assertTrue(response.getStatus() == 404);

    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("SER-SL-STORES_NOT_FOUND"));
  }

  @Test
  public void testSearchStoresByGeoLocNoInput() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.request().get(Response.class);

    // Validate test
    Assert.assertTrue(response.getStatus() == 400);

    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("SER-SL-INVALID_INPUT"));
  }

  @Test
  public void testSearchStoresByGeoLocGood() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{21.315603}).
            queryParam("long", new Object[]{-157.858093}).
            queryParam("radius", new Object[]{10.0}).
            request().get(Response.class);

    // Validate test
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);

    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"09"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"04"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Honolulu"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"HI"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"96818"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Island Paradise"));

    // GeoLocationInfo Check
    Assert.assertTrue(jsonResult.contains("\"latitudeDeg\":21.315603"));
    Assert.assertTrue(jsonResult.contains("\"longitudeDeg\":-157.858093"));
    Assert.assertTrue(jsonResult.contains("\"latitudeRad\":0.3720275"));
    Assert.assertTrue(jsonResult.contains("\"longitudeRad\":-2.755143474"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"SWIM"));
  }

  @Test
  public void testSearchStoresByGeoLocGoodNoRadius() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(SEARCH_BY_GEO_LOCATION_PATH);
    Response response = target.queryParam("lat", new Object[]{21.315603}).
            queryParam("long", new Object[]{-157.858093}).
            request().get(Response.class);

    // Validate test
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);

    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"09"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"04"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Honolulu"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"HI"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"96818"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Island Paradise"));

    // GeoLocationInfo Check
    Assert.assertTrue(jsonResult.contains("\"latitudeDeg\":21.315603"));
    Assert.assertTrue(jsonResult.contains("\"longitudeDeg\":-157.858093"));
    Assert.assertTrue(jsonResult.contains("\"latitudeRad\":0.3720275"));
    Assert.assertTrue(jsonResult.contains("\"longitudeRad\":-2.755143474"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"SWIM"));
  }

  @Test
  public void testFindStoresByStoreIds() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("1127");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    // Validate tests
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    Assert.assertTrue(jsonResult.contains("\"brandId\":\"VSS"));

    // StoreInfo Check
    Assert.assertTrue(jsonResult.contains("\"storeType\":\"09"));
    Assert.assertTrue(jsonResult.contains("\"storeStatus\":\"04"));

    // AddressInfo Check
    Assert.assertTrue(jsonResult.contains("\"city\":\"Honolulu"));
    Assert.assertTrue(jsonResult.contains("\"state\":\"HI"));
    Assert.assertTrue(jsonResult.contains("\"postalCode\":\"96818"));
    Assert.assertTrue(jsonResult.contains("\"mallName\":\"Island Paradise"));

    // GeoLocationInfo Check
    Assert.assertTrue(jsonResult.contains("\"latitudeDeg\":21.315603"));
    Assert.assertTrue(jsonResult.contains("\"longitudeDeg\":-157.858093"));
    Assert.assertTrue(jsonResult.contains("\"latitudeRad\":0.3720275"));
    Assert.assertTrue(jsonResult.contains("\"longitudeRad\":-2.755143474"));

    // Assortments Check
    Assert.assertTrue(jsonResult.contains("assortments\":[\"BEAUTY\",\"LINGERIE\",\"SWIM"));
  }

  @Test
  public void testFindStoresByStoreIdsMultiple() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("734");
    storeIds.add("1127");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    int ctStores = StringUtils.countMatches(jsonResult, "\"storeId\":");
    Assert.assertEquals(ctStores, 2);

    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
  }

  @Test
  public void testFindStoresByStoreIdsNoStoreFound() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("735");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    Assert.assertTrue(response.getStatus() == 404);
  }

  @Test
  public void testFindStoresByStoreIdsNotAllFound() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("17");
    storeIds.add("734");
    storeIds.add("1149");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    int ctStores = StringUtils.countMatches(jsonResult, "\"storeId\":");
    Assert.assertEquals(ctStores, 1);

    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    Assert.assertFalse(jsonResult.contains("\"storeId\":17"));
    Assert.assertFalse(jsonResult.contains("\"storeId\":1149"));
  }

  @Test
  public void testFindStoresByStoreIdsNoStoreIds() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    Assert.assertTrue(response.getStatus() == 400);
    String jsonResult = response.readEntity(String.class);
    Assert.assertFalse(jsonResult.contains("\"storeId\":"));
  }

  @Test
  public void testFindStoresByStoreIdsRepeatId() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("734");
    storeIds.add("1127");
    storeIds.add("734");
    storeIds.add("1127");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    // Validate tests
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    int ct734 = StringUtils.countMatches(jsonResult, "\"storeId\":734");
    Assert.assertEquals(ct734, 1);

    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    int ct1127 = StringUtils.countMatches(jsonResult, "\"storeId\":1127");
    Assert.assertEquals(ct1127, 1);
  }

  @Test
  public void testFindStoresByStoreIdsNonNumeric() {
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(BASE_URI).path(FIND_BY_STORE_IDS_PATH);

    List<String> storeIds = new ArrayList<>();
    storeIds.add("734");
    storeIds.add("1127");
    storeIds.add("88k");
    storeIds.add("1127");
    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response = target.request().post(Entity.json(stores), Response.class);

    // Validate tests
    Assert.assertTrue(response.getStatus() == 200);
    String jsonResult = response.readEntity(String.class);
    Assert.assertTrue(jsonResult.contains("\"storeId\":734"));
    int ct734 = StringUtils.countMatches(jsonResult, "\"storeId\":734");
    Assert.assertEquals(ct734, 1);

    Assert.assertTrue(jsonResult.contains("\"storeId\":1127"));
    int ct1127 = StringUtils.countMatches(jsonResult, "\"storeId\":1127");
    Assert.assertEquals(ct1127, 1);

    Assert.assertFalse(jsonResult.contains("\"storeId\":88k"));
  }


  private static void loadStoreLocatorAssortments() {
    Map<String, List<Integer>> assortmentsMap = new HashMap<>();
    assortmentsMap.put("BEAUTY", Arrays.asList(734, 1127));
    assortmentsMap.put("LINGERIE", Arrays.asList(734, 1127));
    assortmentsMap.put("PINK", Arrays.asList(734, 1127));
    assortmentsMap.put("SWIM", Arrays.asList(734, 1127));
    assortmentsMap.put("VSX", Arrays.asList(734, 1127));

    // Load Hazelcast assortments
    storeLocatorHzInstance.getMap(StoreLocatorCacheNames.STORE_BYASSORTMENTS).putAll(assortmentsMap);
  }

  private static void loadStoreLocatorStoreData() {
    Map<Integer, Store> storeMap = new HashMap<>();

    // Store #1
    Store store = new Store();
    store.setStoreId(734);
    store.setBrandId("VSS");

    // AddressInfo
    AddressInfo addressInfo = new AddressInfo();
    addressInfo.setStreetAddress("7 Lincoln PL");
    addressInfo.setCity("Columbus");
    addressInfo.setState("OH");
    addressInfo.setPostalCode("43211");
    addressInfo.setCountry("US");
    addressInfo.setPhone("(614) 833-7890");
    addressInfo.setMallName("Polaris Parkway");
    store.setAddressInfo(addressInfo);

    // StoreInfo
    StoreInfo storeInfo = new StoreInfo();
    storeInfo.setStoreStatus("01");
    storeInfo.setStoreType("06");
    storeInfo.setOpeningDate(new Date());
    storeInfo.setRemodelDate(new Date());
    storeInfo.setClosingDate(new Date());
    store.setStoreInfo(storeInfo);

    // GeoLocationInfo
    GeoLocationInfo geoLocationInfo = new GeoLocationInfo();
    geoLocationInfo.setLattitudeDeg(39.961180);
    geoLocationInfo.setLongitudeDeg(-82.998790);
    geoLocationInfo.setLattitudeRad(0.6974542);
    geoLocationInfo.setLongitudeRad(-1.448602161);
    store.setGeoLocationInfo(geoLocationInfo);

    // Assortments
    List<String> assortments = new ArrayList<>();
    assortments.add("BEAUTY");
    assortments.add("LINGERIE");
    assortments.add("PINK");
    assortments.add("SWIM");
    assortments.add("VSX");
    store.setAssortments(assortments);

    storeMap.put(store.getStoreId(), store);

    // Store #2
    store = new Store();
    store.setStoreId(1127);
    store.setBrandId("VSS");

    // AddressInfo
    addressInfo = new AddressInfo();
    addressInfo.setStreetAddress("4949 Mall Drive");
    addressInfo.setCity("Honolulu");
    addressInfo.setState("HI");
    addressInfo.setPostalCode("96818");
    addressInfo.setCountry("US");
    addressInfo.setPhone("(808) 478-8000");
    addressInfo.setMallName("Island Paradise");
    store.setAddressInfo(addressInfo);

    // StoreInfo
    storeInfo = new StoreInfo();
    storeInfo.setStoreStatus("04");
    storeInfo.setStoreType("09");
    storeInfo.setOpeningDate(new Date());
    storeInfo.setRemodelDate(null);
    storeInfo.setClosingDate(new Date());
    store.setStoreInfo(storeInfo);

    // GeoLocationInfo
    geoLocationInfo = new GeoLocationInfo();
    geoLocationInfo.setLattitudeDeg(21.315603);
    geoLocationInfo.setLongitudeDeg(-157.858093);
    geoLocationInfo.setLattitudeRad(0.3720275);
    geoLocationInfo.setLongitudeRad(-2.755143474);
    store.setGeoLocationInfo(geoLocationInfo);

    // Assortments
    assortments = new ArrayList<>();
    assortments.add("BEAUTY");
    assortments.add("LINGERIE");
    assortments.add("SWIM");
    store.setAssortments(assortments);

    storeMap.put(store.getStoreId(), store);

    // Load Hazelcast stores
    storeLocatorHzInstance.getMap(StoreLocatorCacheNames.STORE_CORE_INFO).putAll(storeMap);
  }

  private static void loadStoreLocatorOperationalHours() {
    Map<Integer, OperationalInfo> operationalInfoMap = new HashMap<>();

    // Store #1
    OperationalInfo operationalInfo = new OperationalInfo();
    operationalInfo.setLoadDate(new Date());
    operationalInfo.setExtractDate(new Date());
    operationalInfo.setEffectiveDate(new Date());
    operationalInfo.setStoreId(734);

    List<OperationalHours> operationalHourses = new ArrayList<>();
    OperationalHours operationalHours = new OperationalHours();
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.FRIDAY);
    operationalHours.setOpenTime(new Date());
    operationalHours.setCloseTime(new Date());
    operationalHourses.add(operationalHours);

    operationalHours = new OperationalHours();
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.SATURDAY);
    operationalHours.setOpenTime(new Date());
    operationalHours.setCloseTime(new Date());
    operationalHourses.add(operationalHours);

    operationalInfo.setOperationHoursPerDay(operationalHourses);

    // Add to map
    operationalInfoMap.put(operationalInfo.getStoreId(), operationalInfo);


    // Store #2
    operationalInfo = new OperationalInfo();
    operationalInfo.setLoadDate(new Date());
    operationalInfo.setExtractDate(new Date());
    operationalInfo.setEffectiveDate(new Date());
    operationalInfo.setStoreId(1127);

    operationalHourses = new ArrayList<>();
    operationalHours = new OperationalHours();
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.MONDAY);
    operationalHours.setOpenTime(new Date());
    operationalHours.setCloseTime(new Date());
    operationalHourses.add(operationalHours);

    operationalHours = new OperationalHours();
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.WEDNESDAY);
    operationalHours.setOpenTime(new Date());
    operationalHours.setCloseTime(new Date());
    operationalHourses.add(operationalHours);

    operationalInfo.setOperationHoursPerDay(operationalHourses);

    // Add to map
    operationalInfoMap.put(operationalInfo.getStoreId(), operationalInfo);

    // Load Hazelcast assortments
    storeLocatorHzInstance.getMap(StoreLocatorCacheNames.STORE_HOURS_INFO).putAll(operationalInfoMap);
  }

}
